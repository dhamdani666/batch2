<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama','created_by','updated_by'
    ];

    public function get_barang()
    {
        return $this->hasMany('App\Barang','id_category','id');
    }
}
