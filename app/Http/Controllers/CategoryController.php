<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryValidasi;
use Illuminate\Http\Request;
use App\Category;
use Redirect;
use Image;

class CategoryController extends Controller
{
    public function index()
    {
        $category = Category::all();
        return view('category.category',compact('category'));
    }


    public function create()
    {
        return view('category.create');
    }

    public function store(CategoryValidasi $request)
    {

        $tambahDataCategory = Category::create(['nama' => $request->nama,'created_by' => 1, 'updated_by' => 1]);
        if ($tambahDataCategory) {
            return Redirect::to('category')->with("message", "Berhasil Menambah Data Category ");
        } else {
            return Redirect::to('category')->with("messageerror", "Gagal Menambah Data Category ");
        }

    }

    public function show($id)
    {
        $dataCategory = Category::find($id);
        if (!is_null($dataCategory)) {
            return view('category.detail', compact('dataCategory'));
        }
        return Redirect::to('category')->with("messageerror", "Data Category Tidak Tersedia");
    }

    public function edit($id)
    {
        $dataCategory = Category::find($id);
        if (!is_null($dataCategory)) {
            return view('category.edit', compact('dataCategory'));
        }
        return Redirect::to('category')->with("messageerror", "Data Category Tidak Tersedia");

    }


    public function update(CategoryValidasi $request, $id)
    {
        $dataCategory = Category::find($id);
        if (!is_null($dataCategory)) {
            $dataCategory->update(['nama' => $request->nama, 'updated_by' => 1]);
            return Redirect::to("category")->with("message", "Berhasil Mengubah Data Category ");

        }
        return Redirect::to('category')->with("messageerror", "Data Category Tidak Tersedia");

    }

    public function destroy($id)
    {
        $dataCategory = Category::destroy($id);
        if ($dataCategory) {
            return Redirect::to('category')->with("message", "Berhasil Menghapus Data Category ");
        } else {
            return Redirect::to('category')->with("messageerror", "Gagal Menghapus Data Category ");
        }
    }
}
