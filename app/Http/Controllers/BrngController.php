<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\BarangValidasi;
use App\Barang;
use Redirect;
use Image;

class BrngController extends Controller
{
    public function index()
    {
        $barang = Barang::all();
        return view('barang.barang', compact('barang'));
    }


    public function create()
    {

        $category = Category::all();
        return view('barang.create',compact('category'));
    }

    public function store(BarangValidasi $request)
    {

        $gambar = $request->file('gambar');
        $namagambar = 'barang-' . date('His') . '.' . $gambar->getClientOriginalExtension();
        Image::make($gambar)->save('images/barang/' . $namagambar);

        $tambahDataBarang = Barang::create(['id_category' => $request->category, 'nama' => $request->nama, 'deskripsi' => $request->deskripsi, 'stock' => $request->stock, 'gambar' => $namagambar, 'created_by' => 1, 'updated_by' => 1]);
        if ($tambahDataBarang) {
            return Redirect::to('barang')->with("message", "Berhasil Menambah Data Barang ");
        } else {
            return Redirect::to('barang')->with("messageerror", "Gagal Menambah Data Barang ");
        }

    }

    public function show($id)
    {
        $dataBarang = Barang::find($id);
        if (!is_null($dataBarang)) {
            return view('barang.detail', compact('dataBarang'));
        }
        return Redirect::to('barang')->with("messageerror", "Data Barang Tidak Tersedia");
    }

    public function edit($id)
    {
        $dataBarang = Barang::find($id);
        if (!is_null($dataBarang)) {
            $category = Category::all();
            return view('barang.edit', compact('dataBarang','category'));
        }
        return Redirect::to('barang')->with("messageerror", "Data Barang Tidak Tersedia");

    }


    public function update(BarangValidasi $request, $id)
    {
        $dataBarang = Barang::find($id);
        if (!is_null($dataBarang)) {
            if ($request->file('gambar')) {
                $gambar = $request->file('gambar');
                $namagambar = 'barang-' . date('His') . '.' . $gambar->getClientOriginalExtension();
                Image::make($gambar)->save('images/barang/' . $namagambar);
            } else {
                $namagambar = $dataBarang->gambar;
            }

            $dataBarang->update(['id_category' => $request->category, 'nama' => $request->nama, 'deskripsi' => $request->deskripsi, 'stock' => $request->stock, 'gambar' => $namagambar, 'updated_by' => 1]);
            return Redirect::to("barang")->with("message", "Berhasil Mengubah Data Barang ");

        }
        return Redirect::to('barang')->with("messageerror", "Data Barang Tidak Tersedia");

    }

    public function destroy($id)
    {
        $dataBarang = Barang::destroy($id);
        if ($dataBarang) {
            return Redirect::to('barang')->with("message", "Berhasil Menghapus Data Barang ");
        } else {
            return Redirect::to('barang')->with("messageerror", "Gagal Menghapus Data Barang ");
        }
    }
}
