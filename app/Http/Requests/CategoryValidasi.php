<?php
namespace App\Http\Requests;
class CategoryValidasi extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "nama" => "required"
        ];
    }

    public function messages()
    {

        return [
            "nama.required" => "Nama Category Belum Terisi",];
    }

}


