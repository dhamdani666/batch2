<?php
namespace App\Http\Requests;
class UsersValidasi extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    "username" => "required",
                    "password" => "required",
                    "nama" => "required",
                    "profile" => "required"
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    "username" => "required",
                    "nama" => "required",
                ];
            }
            default:break;
        }
    }

    public function messages()
    {

        return [
            "username.required" => "Username Belum Terisi",
            "password.required" => "Password Belum Terisi",
            "nama.required" => "Nama Belum Terisi",
            "profile.required" => "Profile Belum Dimasukan"];
    }

}


