<?php
namespace App\Http\Requests;
class BarangValidasi extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    "category" => "required",
                    "nama" => "required",
                    "stock" => "required",
                    "deskripsi" => "required",
                    "gambar" => "required"
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    "category" => "required",
                    "nama" => "required",
                    "stock" => "required",
                    "deskripsi" => "required"
                ];
            }
            default:break;
        }
    }

    public function messages()
    {

        return [
            "category.required" => "Category Belum Dipilih",
            "nama.required" => "Nama Belum Terisi",
            "stock.required" => "Stock Belum Terisi",
            "deskripsi.required" => "Deskripsi Belum Terisi",
            "gambar.required" => "Gambar Belum Dimasukan"];
    }

}


