<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barang';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_category','nama','deskripsi','stock','gambar','created_by','updated_by'
    ];

    public function get_category()
    {
        return $this->hasOne('App\Category','id','id_category');
    }
}
