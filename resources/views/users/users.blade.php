@extends('template',['title'=>'Users'])
@section('content')
    <div class="text-right">
        <a href="{{ url('users/create') }}" class="btn btn-primary mb-3"><i class="fa fa-plus"></i> New Data</a>
    </div>
    @include('errors.validation')
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>No</th>
                <th>Username</th>
                <th>Nama</th>
                <th>Profile</th>
                <th>Tgl Buat</th>
                <th>User Buat</th>
                <th>User Ubah</th>
                <th>Tgl Ubah</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>

            @foreach($users as $key=>$dataUsers)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$dataUsers->username}}</td>
                    <td>{{$dataUsers->nama}}</td>
                    <td><img src="{{URL('images/users/'.$dataUsers->profile.'')}}" class="img-responsive" width="50"
                             height="50"></td>
                    <td>{{$dataUsers->created_at}}</td>
                    <td>{{$dataUsers->created_by}}</td>
                    <td>{{$dataUsers->updated_at}}</td>
                    <td>{{$dataUsers->updated_by}}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{URL('users/'.$dataUsers->id.'')}}" class="btn btn-sm btn-info">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a href="{{URL('users/'.$dataUsers->id.'/edit')}}" class="btn btn-sm btn-warning">
                                <i class="fa fa-edit"></i>
                            </a>

                            <form action="{{URL('users/'.$dataUsers->id.'')}}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button class="btn btn-sm btn-danger" style="border-radius: 0;"><i
                                            class="far fa-trash-alt"></i></button>
                            </form>

                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection