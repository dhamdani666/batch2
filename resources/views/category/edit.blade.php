@extends('template',['title'=>'Edit Data Category'])
@section('content')
    <div class="card">
        <div class="card-header">
            Edit Data Category
        </div>
        <div class="card-body">
            @include('errors.validation')
            <form action="{{URL('category/'.$dataCategory->id.'')}}" method="POST" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Nama Category</label>
                    <input type="text" name="nama" class="form-control" value="{{$dataCategory->nama}}">
                </div>

                <div class="text-right">
                    <a href="{{url('barang')}}" class="btn btn-danger">
                        <i class="fa fa-times"></i> Batal
                    </a>
                    <button type="submit" class="btn btn-success">
                        <i class="far fa-save"></i> Save Data
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection