@extends('template',['title'=>'Category'])
@section('content')
    <div class="text-right">
        <a href="{{ url('category/create') }}" class="btn btn-primary mb-3"><i class="fa fa-plus"></i> New Data</a>
    </div>
    @include('errors.validation')
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>No</th>
                <th>Nama Category</th>
                <th>Tgl Buat</th>
                <th>User Buat</th>
                <th>User Ubah</th>
                <th>Tgl Ubah</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>

            @foreach($category as $key=>$dataCategory)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$dataCategory->nama}}</td>
                    <td>{{$dataCategory->created_at}}</td>
                    <td>{{$dataCategory->created_by}}</td>
                    <td>{{$dataCategory->updated_at}}</td>
                    <td>{{$dataCategory->updated_by}}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{URL('category/'.$dataCategory->id.'')}}" class="btn btn-sm btn-info">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a href="{{URL('category/'.$dataCategory->id.'/edit')}}" class="btn btn-sm btn-warning">
                                <i class="fa fa-edit"></i>
                            </a>

                            <form action="{{URL('category/'.$dataCategory->id.'')}}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button class="btn btn-sm btn-danger" style="border-radius: 0;"><i
                                            class="far fa-trash-alt"></i></button>
                            </form>

                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection