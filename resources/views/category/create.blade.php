@extends('template',['title'=>'Tambah Data Category'])
@section('content')
    <div class="card">
        <div class="card-header">
            Tambah Data Category
        </div>
        <div class="card-body">
            @include('errors.validation')
            <form action="{{URL('category')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Nama Category</label>
                    <input type="text" name="nama" class="form-control" value="{{old('nama')}}">
                </div>

                <div class="text-right">
                    <a href="{{url('barang')}}" class="btn btn-danger">
                        <i class="fa fa-times"></i> Batal
                    </a>
                    <button type="submit" class="btn btn-success">
                        <i class="far fa-save"></i> Save Data
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection