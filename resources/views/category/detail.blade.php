@extends('template',['title'=>'Detail Data Category'])
@section('content')
    <div class="card">
        <div class="card-header">
            Detail Data Category
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Nama Category</label>
                    <input type="text" name="nama" class="form-control" value="{{$dataCategory->nama}}" readonly>
                </div>

                <div class="col-md-6">
                    <label>List Barang</label>
                    @if(count($dataCategory->get_barang)>0)
                        <div class="list-group">
                            @foreach($dataCategory->get_barang as $dataBarang)
                                <a href="{{URL('barang/'.$dataBarang->id.'')}}" class="list-group-item list-group-item-action">{{$dataBarang->nama}}</a>
                            @endforeach
                        </div>
                    @else
                        <input type="text" class="form-control" value="Belum Tersedia" readonly>
                    @endif


                </div>
            </div>
            <hr>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label>Tanggal Buat</label>
                    <input type="text" name="category" class="form-control" value="{{$dataCategory->created_at}}"
                           readonly>
                </div>
                <div class="form-group col-md-3">
                    <label>User Buat</label>
                    <input type="text" name="nama" class="form-control" value="{{$dataCategory->created_by}}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <label>Tanggal Ubah</label>
                    <input type="text" name="stock" class="form-control" value="{{$dataCategory->updated_at}}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <label>User Ubah</label>
                    <input type="text" name="stock" class="form-control" value="{{$dataCategory->updated_by}}" readonly>
                </div>
            </div>
            <div class="text-right">
                <a href="{{url('barang')}}" class="btn btn-success">
                    <i class="fa fa-chevron-left"></i> Kembali
                </a>
            </div>
        </div>
    </div>
@endsection