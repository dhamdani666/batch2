@extends('template',['title'=>'Barang'])
@section('content')
    <div class="text-right">
        <a href="{{ url('barang/create') }}" class="btn btn-primary mb-3"><i class="fa fa-plus"></i> New Data</a>
    </div>
    @include('errors.validation')
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>No</th>
                <th>Category</th>
                <th>Nama</th>
                <th>Deskripsi</th>
                <th>Stock</th>
                <th>Gambar</th>
                <th>Tgl Buat</th>
                <th>User Buat</th>
                <th>User Ubah</th>
                <th>Tgl Ubah</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>

            @foreach($barang as $key=>$dataBarang)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{!empty($dataBarang->get_category) ? $dataBarang->get_category->nama : '-'}}</td>
                    <td>{{$dataBarang->nama}}</td>
                    <td>{{$dataBarang->deskripsi}}</td>
                    <td>{{$dataBarang->stock}}</td>
                    <td><img src="{{URL('images/barang/'.$dataBarang->gambar.'')}}" class="img-responsive" width="50"
                             height="50"></td>
                    <td>{{$dataBarang->created_at}}</td>
                    <td>{{$dataBarang->created_by}}</td>
                    <td>{{$dataBarang->updated_at}}</td>
                    <td>{{$dataBarang->updated_by}}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{URL('barang/'.$dataBarang->id.'')}}" class="btn btn-sm btn-info">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a href="{{URL('barang/'.$dataBarang->id.'/edit')}}" class="btn btn-sm btn-warning">
                                <i class="fa fa-edit"></i>
                            </a>

                            <form action="{{URL('barang/'.$dataBarang->id.'')}}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button class="btn btn-sm btn-danger" style="border-radius: 0;"><i
                                            class="far fa-trash-alt"></i></button>
                            </form>

                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection