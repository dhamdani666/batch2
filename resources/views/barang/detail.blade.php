@extends('template',['title'=>'Detail Data Barang'])
@section('content')
    <div class="card">
        <div class="card-header">
            Detail Data Barang
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label>Category</label>
                    <input type="text" name="category" class="form-control" value="{{!empty($dataBarang->get_category) ? $dataBarang->get_category->nama : '-'}}" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>Nama Barang</label>
                    <input type="text" name="nama" class="form-control" value="{{$dataBarang->nama}}" readonly>
                </div>
                <div class="form-group col-md-2">
                    <label>Stock Barang</label>
                    <input type="text" name="stock" class="form-control" value="{{$dataBarang->stock}}" readonly>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Deskripsi Barang</label>
                    <textarea type="text" name="deskripsi" class="form-control"
                              readonly>{{$dataBarang->deskripsi}}</textarea>
                </div>
                <div class="form-group col-md-6">
                    <label>Gambar Barang</label> <br>
                    <img src="{{URL('images/barang/'.$dataBarang->gambar.'')}}" class="img-responsive" width="150"
                         height="150">
                </div>
            </div>
            <hr>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label>Tanggal Buat</label>
                    <input type="text" name="category" class="form-control" value="{{$dataBarang->created_at}}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <label>User Buat</label>
                    <input type="text" name="nama" class="form-control" value="{{$dataBarang->created_by}}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <label>Tanggal Ubah</label>
                    <input type="text" name="stock" class="form-control" value="{{$dataBarang->updated_at}}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <label>User Ubah</label>
                    <input type="text" name="stock" class="form-control" value="{{$dataBarang->updated_by}}" readonly>
                </div>
            </div>
            <div class="text-right">
                <a href="{{url('barang')}}" class="btn btn-success">
                    <i class="fa fa-chevron-left"></i> Kembali
                </a>
            </div>
        </div>
    </div>
@endsection