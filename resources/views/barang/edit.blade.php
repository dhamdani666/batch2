@extends('template',['title'=>'Edit Data Barang'])
@section('content')
    <div class="card">
        <div class="card-header">
            Edit Data Barang
        </div>
        <div class="card-body">
            @include('errors.validation')
            <form action="{{URL('barang/'.$dataBarang->id.'')}}" method="POST" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>Category</label>
                        <select name="category" class="form-control">
                            <option value="">Pilih Category</option>
                            @foreach($category as $dataCategory)
                                <option value="{{$dataCategory->id}}" @if(old('category')==null?$dataBarang->id_category:old('category')==$dataCategory->id) selected @endif>{{$dataCategory->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Nama Barang</label>
                        <input type="text" name="nama" class="form-control" value="{{$dataBarang->nama}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label>Stock Barang</label>
                        <input type="text" name="stock" class="form-control" value="{{$dataBarang->stock}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Deskripsi Barang</label>
                        <textarea type="text" name="deskripsi" class="form-control"
                        >{{$dataBarang->deskripsi}}</textarea>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Gambar Barang</label> <br>
                        <input type="file" name="gambar" class="form-control">
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{url('barang')}}" class="btn btn-danger">
                        <i class="fa fa-times"></i> Batal
                    </a>
                    <button type="submit" class="btn btn-success">
                        <i class="far fa-save"></i> Save Data
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection